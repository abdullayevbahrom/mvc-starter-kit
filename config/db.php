<?php

return [
    "enable" => true,
    "host" => 'mysql',
    "username" => 'stock',
    "password" => 'secret',
    "db_name" => 'stock'
];